package annotations;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class a {

	@BeforeMethod
	public void beforeMethod(){
		System.out.println("beforemethod");
	}
	

	
	@BeforeClass
	public void beforeClass(){
		System.out.println("In beforeClass");
	}
	
	
	@BeforeTest
	public void beforeTest(){
		System.out.println("Beforetest");
	}
	
	
	@Test(groups="smoke", description="This is the sample test to demonstarte testng test annoatation")
	public void test1(){
		System.out.println("In test 1.");
	}
	
	@Test(groups="regression")
	public void test2(){
		System.out.println("In test 2.");
	}
	
	@Test(groups="regression")
	public void test3(){
		System.out.println("In test 3.");
	}
	
	@Test(groups="regression")
	public void test4(){
		System.out.println("In test 4.");
	}
	
	@Test(groups="smoke")
	public void test5(){
		System.out.println("In test 5.");
	}
	
}

package annotations;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class b {

	@BeforeSuite
	public void beforeSuite(){
		System.out.println("beforeSuite - B");
	}
	@Test
	public void test6(){
		System.out.println("In test 6.");
	}
	
	@Test(groups="regression")
	public void test7(){
		System.out.println("In test 7.");
	}
	
	@Test(groups="regression")
	public void test8(){
		System.out.println("In test 8.");
	}
	
	
	@AfterMethod
	public void afterMethod(){
		System.out.println("In after method");
	}
	
	
	@AfterSuite
	public void afterSuite(){
		System.out.println("In after suite");
	}
}

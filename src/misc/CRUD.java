package misc;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CRUD {
	
	@Test(priority=1, groups="regression")
	public void createUser(){
		System.out.println("In create user test");
		
		Assert.assertTrue(true,"Could not create the user");
	}
	
	@Test(priority=2, dependsOnMethods="createUser", groups="regression")
	public void readUser(){
		System.out.println("In read user test");
	}
	
	@Test(priority=3,dependsOnMethods="createUser", groups="regression")
	public void editUser(){
		System.out.println("In edit user test");
	}

	
	@Test(priority=4,dependsOnMethods="createUser", groups="regression")
	public void deleteUser(){
		System.out.println("In delete user test");
	}
}


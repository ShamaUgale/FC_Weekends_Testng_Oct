package params;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class registrationTest {
	
	
	@Test(dataProvider="getData",groups="regression")
	public void registerTest(String fname, String lname, String email, String phone){
		System.out.println("First name : "+ fname);
		System.out.println("Last name : "+ lname);
		System.out.println("Email "+ email);
		System.out.println("Phone : "+ phone);
		System.out.println("*********************************");
	}
	
	
	
	@DataProvider
	public Object[][] getData(){
		Object[][] data= new Object[10][4];
		
		data[0][0]="Fname 1";
		data[0][1]="Lname 1";
		data[0][2]="Email 1";
		data[0][3]="Phone 1";
		
		data[1][0]="Fname 2";
		data[1][1]="Lname 2";
		data[1][2]="Email 2";
		data[1][3]="Phone 2";
		
		data[2][0]="Fname 3";
		data[2][1]="Lname 3";
		data[2][2]="Email 3";
		data[2][3]="Phone 3";
		
		data[3][0]="Fname 4";
		data[3][1]="Lname 4";
		data[3][2]="Email 4";
		data[3][3]="Phone 4";
		
		data[4][0]="Fname 5";
		data[4][1]="Lname 5";
		data[4][2]="Email 5";
		data[4][3]="Phone 5";
		
		data[5][0]="Fname 6";
		data[5][1]="Lname 6";
		data[5][2]="Email 6";
		data[5][3]="Phone 6";
		
		data[6][0]="Fname 7";
		data[6][1]="Lname 7";
		data[6][2]="Email 7";
		data[6][3]="Phone 7";
		
		data[7][0]="Fname 8";
		data[7][1]="Lname 8";
		data[7][2]="Email 8";
		data[7][3]="Phone 8";
		
		data[8][0]="Fname 9";
		data[8][1]="Lname 9";
		data[8][2]="Email 9";
		data[8][3]="Phone 9";
		
		
		data[9][0]="Fname 10";
		data[9][1]="Lname 10";
		data[9][2]="Email 10";
		data[9][3]="Phone 10";
		
		return data;
	}

}
